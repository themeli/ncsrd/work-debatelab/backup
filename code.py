from transformers import BertTokenizer, BertForTokenClassification
import torch

data =  "Today is a good day my dude"
data = data.split()
labels = [1] * len(data)
print(data)

# make tokenizer
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

# tokenize each word
tokens = []
tok_labels = []
for word, label in zip(data, labels):
    # tokenize ingoring special tokens
    tokenized = tokenizer(word, add_special_tokens=False)["input_ids"]
    tokens.extend(tokenized)
    tok_labels.extend([label] * len(tokenized))

# adding cls & sep probably not needed
# maybe add [SEP] between sentences?
# https://huggingface.co/transformers/main_classes/tokenizer.html
# tokenizer.cls_token
# tokenizer.sep_token

tokens = torch.LongTensor([tokens])
tok_labels = torch.LongTensor([tok_labels])
print("Inputs shape:")
print(tokens.shape)
print(tok_labels.shape)



# Model
model = BertForTokenClassification.from_pretrained('bert-base-uncased')

outputs = model(input_ids=tokens, labels=tok_labels)
print(outputs)


