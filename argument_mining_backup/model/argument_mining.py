import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn.functional as f
import torch.utils.data as torch_data
from sklearn.metrics import accuracy_score
from transformers import BertForTokenClassification

from backup.utils import utils
from backup.model import ArgumentMiningDataset
from backup.model.feed_forward import FeedForward
from backup.model.crf_model import CRFLayer


class ArgumentMiningClassifier(pl.LightningModule):

    def __init__(self, properties, logger, encoded_labels, datasets):
        super(ArgumentMiningClassifier, self).__init__()
        self.app_logger = logger
        self.properties = properties
        self.device_name = "cuda:1" if torch.cuda.is_available() else "cpu"
        self.encoded_labels = encoded_labels
        self.num_labels = len(encoded_labels.keys())

        self.total_data = datasets["total_data"]
        self.total_labels = datasets["total_labels"]

        self.train_data = utils.convert_ndarray_to_tensor(datasets["train_data"], device_name=self.device_name)
        self.train_labels = utils.convert_ndarray_to_tensor(datasets["train_labels"], device_name=self.device_name)
        self.test_data = utils.convert_ndarray_to_tensor(datasets["test_data"], device_name=self.device_name)
        self.test_labels = utils.convert_ndarray_to_tensor(datasets["test_labels"], device_name=self.device_name)
        self.validation_data = utils.convert_ndarray_to_tensor(datasets["validation_data"],
                                                               device_name=self.device_name)
        self.validation_labels = utils.convert_ndarray_to_tensor(datasets["validation_labels"],
                                                                 device_name=self.device_name)

        self.validation_output = None
        self.batch_validation_accuracies = []
        self.fold_validation_accuracies = []
        self.test_output = None
        self.test_accuracies = []
        self.test_accuracy = 0.0

        self.bert_model = BertForTokenClassification.from_pretrained('nlpaueb/bert-base-greek-uncased-v1',
                                                                     num_labels=self.num_labels).to(self.device_name)
        self.ff = FeedForward(properties=properties, logger=logger, device_name=self.device_name,
                              num_labels=self.num_labels)
        self.crf = CRFLayer(properties=properties, logger=logger, num_labels=self.num_labels,
                            device_name=self.device_name, encoded_labels=encoded_labels)

    def train_dataloader(self):
        self.app_logger.debug("Creating train DataLoader")
        train_dataset = ArgumentMiningDataset(data=self.train_data, labels=self.train_labels)
        return torch_data.DataLoader(train_dataset, batch_size=self.properties["batch_size"])

    def val_dataloader(self):
        self.app_logger.debug("Creating validation DataLoader")
        validation_dataset = ArgumentMiningDataset(data=self.validation_data, labels=self.validation_labels)
        return torch_data.DataLoader(validation_dataset, batch_size=self.properties["batch_size"])

    def test_dataloader(self):
        self.app_logger.debug("Creating test DataLoader")
        test_dataset = ArgumentMiningDataset(data=self.test_data, labels=self.test_labels)
        return torch_data.DataLoader(test_dataset, batch_size=self.properties["batch_size"])

    def forward(self, tokens, labels):
        # in lightning, forward defines the prediction/inference actions
        self.app_logger.debug("Start forward")
        self.app_logger.debug("Start BERT training")
        bert_output = self.bert_model(input_ids=tokens, output_hidden_states=True)
        logits = bert_output[0]
        self.app_logger.debug("Bert output shape: {}".format(logits.shape))
        embeddings = bert_output[1][-1]
        self.app_logger.debug("Start FF training")
        ff_output = self.ff.forward(embeddings)
        self.app_logger.debug("FF output shape: {}".format(ff_output.shape))
        self.app_logger.debug("Starting CRF training")
        mask = tokens != 0
        output = self.crf.forward(logits=ff_output, labels=labels, mask=mask)
        return output

    def training_step(self, batch, batch_idx):
        # training_step defined the train loop. It is independent of forward
        x, y = batch
        x = x.squeeze().to(self.device_name)
        y = y.squeeze().to(self.device_name)
        if len(x.shape) == 1:
            x = x.reshape(1, x.shape[0])
            y = y.reshape(1, y.shape[0])
        self.app_logger.debug("Batch idx: {}".format(batch_idx))
        self.app_logger.debug("Input shape: {}".format(x.shape))
        self.app_logger.debug("Labels shape: {}".format(y.shape))
        output = self.forward(tokens=x, labels=y)
        loss = output["loss"]
        self.app_logger.debug("Training step loss: {}".format(loss))
        logs = {"train_loss": loss}
        # output = torch.reshape(output["sequence_of_tags"], (-1, 7)).to(self.device_name)
        output = output["sequence_of_tags"]
        y = y.flatten()
        y_true = y.to("cpu")
        y_true = y_true.numpy()
        y_pred = np.asarray(output)
        y_pred = y_pred.reshape((y_pred.shape[0] * y_pred.shape[1],))
        # loss = self.loss_function(logits=output, true_labels=y)
        # output = torch.argmax(output, dim=1)
        accuracy = accuracy_score(y_true=y_true, y_pred=y_pred)
        correct = (y_true == y_pred).sum()
        total = y.shape[0]
        batch_dictionary = {
            # REQUIRED: It ie required for us to return "loss"
            "loss": loss,
            # optional for batch logging purposes
            "log": logs,
            # info to be used at epoch end
            "correct": correct,
            "total": total,
            "accuracy": accuracy
        }
        return batch_dictionary

    def validation_step(self, batch, batch_idx):
        x, y = batch
        x = x.squeeze().to(self.device_name)
        y = y.squeeze().to(self.device_name)
        output = self.forward(tokens=x, labels=y)
        self.validation_output = output["sequence_of_tags"]
        loss = output["loss"]
        self.app_logger.debug("Validation step loss: {}".format(loss))
        logs = {"train_loss": loss}
        # self.validation_output = torch.reshape(self.validation_output, (-1, 7)).to(self.device_name)
        y = y.flatten()
        # loss = self.loss_function(logits=self.validation_output, true_labels=y)
        # output = torch.argmax(self.validation_output, dim=1)
        y_true = y.to("cpu")
        y_true = y_true.numpy()
        y_pred = np.asarray(self.validation_output)
        y_pred = y_pred.reshape((y_pred.shape[0] * y_pred.shape[1],))
        accuracy = accuracy_score(y_true=y_true, y_pred=y_pred)
        self.batch_validation_accuracies.append(accuracy)
        correct = (y_true == y_pred).sum()
        total = y.shape[0]
        batch_dictionary = {
            # REQUIRED: It ie required for us to return "loss"
            "loss": loss,
            # optional for batch logging purposes
            "log": logs,
            # info to be used at epoch end
            "correct": correct,
            "total": total,
            "accuracy": accuracy
        }
        return batch_dictionary

    def test_step(self, batch, batch_idx):
        x, y = batch
        x = x.squeeze().to(self.device_name)
        y = y.squeeze().to(self.device_name)
        output = self.forward(tokens=x, labels=y)
        self.test_output = output["sequence_of_tags"]
        loss = output["loss"]
        self.app_logger.debug("Test step loss: {}".format(loss))
        logs = {"train_loss": loss}
        y = y.flatten()
        # self.test_output = torch.reshape(self.test_output, (-1, 7)).to(self.device_name)
        # loss = self.loss_function(logits=self.test_output, true_labels=y)
        y_true = y.to("cpu")
        y_true = y_true.numpy()
        y_pred = np.asarray(self.test_output)
        y_pred = y_pred.reshape((y_pred.shape[0] * y_pred.shape[1],))
        accuracy = accuracy_score(y_true=y_true, y_pred=y_pred)
        self.test_accuracies.append(accuracy)
        correct = (y_true == y_pred).sum()
        total = y.shape[0]
        batch_dictionary = {
            # REQUIRED: It ie required for us to return "loss"
            "loss": loss,
            # optional for batch logging purposes
            "log": logs,
            # info to be used at epoch end
            "correct": correct,
            "total": total,
            "accuracy": accuracy
        }
        return batch_dictionary

    def loss_function(self, logits, true_labels):
        loss_function_name = self.properties.get("loss", "cross_entropy")
        if loss_function_name == "cross_entropy":
            loss = f.cross_entropy(input=logits, target=true_labels)
            # add more loss functions
        else:
            loss = f.cross_entropy(input=logits, target=true_labels)
        return loss

    def configure_optimizers(self):
        optimizer_name = self.properties.get("optimizer", "Adam")
        learning_rate = self.properties.get("learning_rate", 1e-3)
        if optimizer_name == "Adam":
            optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)
        elif optimizer_name == "SGD":
            optimizer = torch.optim.SGD(self.parameters(), lr=learning_rate)
        else:
            optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)
        return optimizer
