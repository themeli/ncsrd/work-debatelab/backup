import torch
from torchcrf import CRF


class CRFLayer(torch.nn.Module):

    def __init__(self, properties, logger, device_name, num_labels, encoded_labels):
        super(CRFLayer, self).__init__()
        self.num_labels = num_labels
        self.properties = properties
        self.app_logger = logger
        self.device_name = device_name
        self.encoded_labels = encoded_labels
        self.crf = CRF(num_tags=num_labels).to(self.device_name)

    def forward(self, logits, labels, mask):
        """
        def forward(
        self, inputs: torch.Tensor, tags: torch.Tensor, mask: torch.BoolTensor = None) -> torch.Tensor
        """
        log_likelihood = self.crf.forward(emissions=logits, tags=labels, mask=mask)
        sequence_of_tags = self.crf.decode(logits)
        self.app_logger.debug("CRF sequence output: {}".format(sequence_of_tags))
        loss = -1 * log_likelihood  # Log likelihood is not normalized (It is not divided by the batch size).

        # best_tag_sequence --> List with len=batch_size, each list item --> tuple of 2 elements
        # first tuple element --> list of sequence length size --> each item --> label (e.g. 2)
        # best_tag_sequence = self.crf.viterbi_tags(logits, mask)
        # class_probabilities = torch.zeros(logits.shape[0], self.properties["max_len"], self.num_labels)
        # for i, batch_tuple in enumerate(best_tag_sequence):
        #     tags_list = batch_tuple[0]
        #     for j, tag in enumerate(tags_list):
        #         class_probabilities[i, j, tag] = 1.0
        return {"loss": loss, "sequence_of_tags": sequence_of_tags}
