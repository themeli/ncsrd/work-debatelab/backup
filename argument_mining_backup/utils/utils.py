import json
import pickle
from os.path import join

import torch

from data.data import Document, Segment, Relation


def convert_ndarray_to_tensor(array, device_name):
    tensors = []
    for ar in array:
        dat = torch.LongTensor([ar]).to(device_name)
        tensors.append(dat)
    return tensors


def load_data(resources_folder, filename, pickle_filename):
    path = join(resources_folder, filename)
    with open(path, "r") as f:
        content = json.loads(f.read())
    documents = content["data"]["documents"]
    docs = []
    for doc in documents:
        document = Document(document_id=doc["id"], name=doc["name"], content=doc["text"],
                            annotations=doc["annotations"])
        for annotation in document.annotations:
            annotation_id = annotation["_id"]
            spans = annotation["spans"]
            if len(spans) > 1:
                continue
            segment_type = annotation["type"]
            span = spans[0]
            attributes = annotation["attributes"]
            if segment_type == "argument":
                segment_text = span["segment"]
                segment = Segment(segment_id=annotation_id, document_id=document.document_id, text=segment_text,
                                  char_start=span["start"], char_end=span["end"], arg_type=attributes["type"])
                document.segments.append(segment)
            elif segment_type == "argument_relation":
                relation_type, kind, arg1_id, arg2_id = "", "", "", ""
                arg1, arg2 = None, None
                for attribute in attributes:
                    name = attribute["name"]
                    value = attribute["value"]
                    if name == "type":
                        relation_type = value
                        if relation_type == "support" or relation_type == "attack":
                            kind = "relation"
                        else:
                            kind = "stance"
                    elif name == "arg1":
                        arg1_id = value
                    elif name == "arg2":
                        arg2_id = value
                for seg in document.segments:
                    if seg.segment_id == arg1_id:
                        arg1 = seg
                    elif seg.segment_id == arg2_id:
                        arg2 = seg

                relation = Relation(relation_id=annotation_id, document_id=document.document_id, arg1=arg1, arg2=arg2,
                                    kind=kind, relation_type=relation_type)
                if kind == "relation":
                    document.relations.append(relation)
                else:
                    document.stance.append(relation)
        docs.append(doc)
    with open(join(resources_folder, pickle_filename), "wb") as f:
        pickle.dump(docs, f)
