import pickle
import traceback
from datetime import datetime
from os import getcwd
from os.path import join, exists

import numpy as np
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping, ProgressBar
from pytorch_lightning.loggers import TensorBoardLogger
from sklearn.model_selection import train_test_split, KFold

from backup.utils import AppConfig
from backup.model.argument_mining import ArgumentMiningClassifier
from backup.data.preprocessing import DataPreprocessor
from backup.data import DataTransform


def get_data(app_config):
    logger = app_config.app_logger
    if not exists(join(app_config.resources_path, app_config.pickle_filename)):
        logger.info("Documents pickle does not exist. Starting data preprocessing flow")
        data_preprocessor = DataPreprocessor(data_path=app_config.data_path,
                                             processed_data_path=app_config.processed_data_path,
                                             properties=app_config.properties, logger=logger)
        data_preprocessor.bio_tagging(resources_path=app_config.resources_path,
                                      pickle_filename=app_config.pickle_filename)
        documents = data_preprocessor.documents
    else:
        logger.info("Documents pickle exists. Loading documents...")
        with open(join(app_config.resources_path, app_config.pickle_filename), "rb") as f:
            documents = pickle.load(f)
    return documents


def get_encoded_data(app_config):
    logger = app_config.app_logger
    properties = app_config.properties
    if not exists(join(app_config.resources_path, app_config.pickle_encoded_filename)):
        logger.info("Encoded data do not exist. Create encoded input for classifier")
        documents = get_data(app_config=app_config)
        logger.info("Data are loaded!")
        instance_kind = properties.get("instance_kind", None)
        data_transform = DataTransform(documents=documents, logger=logger, instance_kind=instance_kind)
        pad_token = properties.get("pad_token", 0)
        max_len = properties.get("max_len", 512)
        data = data_transform.encode(path=app_config.resources_path, pickle_file=app_config.pickle_encoded_filename,
                                     max_len=max_len, pad_token=pad_token)
    else:
        logger.info("Found encoded data. Loading data...")
        with open(join(app_config.resources_path, app_config.pickle_encoded_filename), "rb") as f:
            data = pickle.load(f)
    return data


def create_model_and_trainer(data, app_config):
    logger = app_config.app_logger
    properties = app_config.properties
    logger.debug("Preparing train and test datasets")
    test_size = properties.get("test_size", 0.2)
    tokens = np.asarray([d.numpy().flatten() for d in data[0]])
    labels = np.asarray([d.numpy().flatten() for d in data[1]])
    train_data, test_data, train_labels, test_labels = train_test_split(tokens, labels, test_size=test_size,
                                                                        random_state=0)
    splits = properties.get("splits", 10)
    k_fold = KFold(n_splits=splits)
    classifiers = []
    trainers = []
    fold_counter = 0
    for train_index, test_index in k_fold.split(train_data, train_labels):
        fold_counter += 1
        logger.debug("Fold counter: {}".format(fold_counter))
        print(train_index, test_index)

        x_train_fold = train_data[train_index]
        y_train_fold = train_labels[train_index]
        x_validation_fold = train_data[test_index]
        y_validation_fold = train_labels[test_index]

        logger.debug("Train data shape: {}".format(x_train_fold.shape))
        logger.debug("Train labels shape: {}".format(y_train_fold.shape))
        logger.debug("Validation data shape: {}".format(x_validation_fold.shape))
        logger.debug("Validation labels shape: {}".format(y_validation_fold.shape))

        datasets = {
            "total_data": data[0],
            "total_labels": data[1],
            "train_data": x_train_fold,
            "train_labels": y_train_fold,
            "test_data": test_data,
            "test_labels": test_labels,
            "validation_data": x_validation_fold,
            "validation_labels": y_validation_fold
        }
        classifier = ArgumentMiningClassifier(properties=properties, logger=logger, encoded_labels=data[2],
                                              datasets=datasets)
        epochs = properties.get("epochs", 100)
        checkpoint_callback = ModelCheckpoint(
            filepath=join(app_config.model_path, app_config.run),
            verbose=True,
            monitor="train_loss",
            mode='min'
        )
        path = join(app_config.tensorboard_path, app_config.run)
        tb_logger = TensorBoardLogger(path, name="argument_mining")
        trainer = Trainer(max_epochs=epochs, min_epochs=1, checkpoint_callback=True, logger=tb_logger,
                          callbacks=[EarlyStopping(monitor="train_loss"), ProgressBar(), checkpoint_callback])
        trainer.fit(classifier)
        classifier.fold_validation_accuracies.append(np.mean(classifier.batch_validation_accuracies))
        classifiers.append(classifier)
        trainers.append(trainer)
    timestamp = datetime.now()
    models_file = "classifiers_data_{}".format(timestamp)
    with open(join(app_config.output_path, models_file), "wb") as f:
        pickle.dump(classifiers, f)
    for classifier, trainer in zip(classifiers, trainers):
        logger.debug("Testing classifier {}".format(classifiers.index(classifier)))
        trainer.test(classifier)
        classifier.test_accuracy = np.mean(classifier.test_accuracies)
    return classifiers


def main():
    app_path = join(getcwd(), "app") if getcwd().endswith("argument_mining") else getcwd()
    app_config = AppConfig(app_path=app_path)
    logger = app_config.app_logger
    try:
        logger.info("Loading data")
        data = get_encoded_data(app_config=app_config)
        print(data[2])
        logger.info("Data are loaded!")
        classifiers = create_model_and_trainer(data=data, app_config=app_config)

        test_accuracies = [c.test_accuracy for c in classifiers]
        max_accuracy = max(test_accuracies)
        app_config.send_email(body="Run finished with test accuracy: {}".format(max_accuracy))

    except(Exception, BaseException) as e:
        logger.error("Error occurred: {}".format(traceback.format_exc()))
        app_config.send_email(body="An error occurred during the run!{}".format(e))


if __name__ == '__main__':
    main()
