class Segment:
    id = ""
    text = ""
    type = ""
    tokens = []
    labels = []
    stance = []
    logger = None
    relative_index = -1

    def __init__(self, text, segment_type, logger, segment_id=""):
        self.id = segment_id
        self.text = text
        self.type = segment_type
        self.logger = logger
        self.tokens = []
        self.labels = []
        self.relative_index = []

    def preprocess_segment(self, segments, segment_index, indices, text, tmp_text, repl_char, final_segments):
        self.logger.debug("Checking processing for segment: {}".format(self.text))
        if self._do_preprocess(segments=segments, text=text, segment_index=segment_index):
            self.logger.debug("Segment can be processed!")
            for i, token in enumerate(self.text.split()):
                self.tokens.append(token)
                if i == 0:
                    self.labels.append("B-{}".format(self.type))
                else:
                    self.labels.append("I-{}".format(self.type))
            self.relative_index = text.index(self.text)
            indices.append(self.relative_index)
            self.logger.debug("Tokens, labels, indices are updated")
            tmp_text = tmp_text.replace(self.text, repl_char * len(self.text))
            self.logger.debug("New temp text: {}".format(tmp_text))
            final_segments.append(self)
        return tmp_text, indices, final_segments

    def _do_preprocess(self, segments, text, segment_index):
        segment_ok = True
        for idx, checking_segment in enumerate(segments):
            self.logger.debug("Comparing with segment : {}".format(checking_segment.text))

            if segment_index == idx:
                continue

            is_contained = self.text in checking_segment.text
            if not is_contained:
                continue

            is_smaller = len(checking_segment.text) > len(self.text)
            if not is_smaller:
                continue

            current_segment_starts = text.index(self.text)
            current_segment_ends = current_segment_starts + len(self.text)
            checking_segment_starts = text.index(checking_segment.text)
            checking_segment_ends = checking_segment_starts + len(checking_segment.text)
            is_idx_between = current_segment_starts >= checking_segment_starts and \
                             current_segment_ends <= checking_segment_ends
            if not is_idx_between:
                continue
            segment_ok = False
        return segment_ok
