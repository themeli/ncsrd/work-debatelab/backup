import string

import numpy as np

from backup.data.relations import Relation
from backup.data import Segment
from backup.data.stance import Stance


class Document:
    filename = ""
    text = ""
    segments = []
    relations = []
    indices = []
    data = {}
    logger = None

    def __init__(self, filename, logger):
        self.logger = logger
        self.filename = filename
        self.segments = []
        self.relations = []
        self.indices = []

    def preprocess_document(self, data, repl_char="="):
        self.data = data
        self.text = self.data["content"]
        tmp_text = self.text
        tmp_text = self._init_arguments(tmp_text=tmp_text, repl_char=repl_char)
        self.logger.debug("Finished argument preprocessing")
        self.logger.debug("Start BIO-tagging for non-argumentative segments")
        self._init_non_arguments(tmp_text=tmp_text, repl_char=repl_char)
        self._fix_document_order()
        self._find_relations()

    def _init_arguments(self, tmp_text, repl_char="="):
        for dat in self.data["annotations"]["ADUs"]:
            seg = dat["segment"]
            self.logger.debug("Segment: {}".format(seg))
            if type(seg) == float:
                seg = None
            if not seg or seg.lower() == "NaN".lower():
                continue
            segment = Segment(segment_id=dat["id"], text=seg, segment_type=dat["type"], logger=self.logger)
            if dat.get("stance"):
                for s in dat.get("stance"):
                    segment.stance = Stance(stance_id=s["id"], stance_type=s["type"])
            self.segments.append(segment)
        final_segments = []
        for segment_index, segment in enumerate(self.segments):
            self.logger.debug("Document text: {}".format(self.text))
            self.logger.debug("Replaced text: {}".format(tmp_text))
            self.logger.debug("Processing segment: {}".format(segment.text))
            tmp_text, self.indices, final_segments = segment.preprocess_segment(segments=self.segments,
                                                                                indices=self.indices,
                                                                                segment_index=segment_index,
                                                                                text=self.text,
                                                                                tmp_text=tmp_text, repl_char=repl_char,
                                                                                final_segments=final_segments)
        self.segments = final_segments
        return tmp_text

    def _init_non_arguments(self, tmp_text, repl_char="="):
        # drop zero-length remainders or punctuation-only
        punct = str.maketrans('', '', string.punctuation)
        remainders = [x.strip() for x in tmp_text.split(repl_char)]
        remainders = [x for x in remainders if len(x.translate(punct)) > 0]
        for rem in remainders:
            self.logger.debug("Checking non-arg segment: {}".format(rem))
            segment = Segment(text=rem, segment_type="O", logger=self.logger)
            segment.relative_index = self.text.index(rem)
            for token in rem.split():
                segment.tokens.append(token)
                segment.labels.append("O")
            self.segments.append(segment)
            self.indices.append(segment.relative_index)

    def _find_relations(self):
        for rel in self.data["annotations"]["Relations"]:
            relation = Relation(relation_id=rel["id"], relation=rel["type"])
            arg1 = rel["arg1"]
            arg2 = rel["arg2"]
            count = 0
            for segment in self.segments:
                if segment.id == arg1:
                    relation.segment1 = segment
                    count += 1
                elif segment.id == arg2:
                    relation.segment2 = segment
                    count += 1
                if count == 2:
                    break
            self.relations.append(relation)

    def _fix_document_order(self):
        # annotation done -- reorder
        ix = np.argsort(self.indices)
        reordered_segments = []
        reordered_indices = []
        # reorder the actual data
        for i in ix:
            reordered_segments.append(self.segments[i])
            reordered_indices.append(self.indices[i])
        self.segments = reordered_segments
        self.indices = reordered_indices
