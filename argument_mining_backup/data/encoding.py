import pickle
from os.path import join

import numpy as np
import torch
from transformers import BertTokenizer


class DataTransform:
    documents = []
    instances = []
    encoded_labels = {}
    logger = None

    def __init__(self, documents, logger, instance_kind="document"):
        self.documents = documents
        self.logger = logger
        self._transform(kind=instance_kind)
        self._transform_labels()

    def _transform(self, kind):
        for document in self.documents:
            if kind == "document":
                tokens, labels = [], []
                for segment in document.segments:
                    tokens += segment.tokens
                    labels += segment.labels
                self.instances.append((tokens, labels))
            elif kind == "segment_pairs":
                pass
            else:
                self.logger.error("Invalid instance kind {}!".format(kind))

    def _transform_labels(self):
        collected_labels = []
        for instance in self.instances:
            collected_labels.extend(instance[1])
        collected_labels = set(collected_labels)
        for i, label in enumerate(collected_labels):
            self.encoded_labels[label] = i
        for idx, instance in enumerate(self.instances):
            labels = instance[1]
            for i, label in enumerate(labels):
                labels[i] = self.encoded_labels[label]
            self.instances[idx] = (instance[0], labels)

    def _labels_to_one_hot(self, total_labels):
        num_labels = len(self.encoded_labels.keys())
        new_total_labels = []
        for idx, labels in enumerate(total_labels):
            labels = labels.numpy()[0]
            new_labels = []
            for label in labels:
                one_hot = np.zeros((num_labels,))
                one_hot[label] = 1
                new_labels.append(one_hot)
            new_labels = torch.LongTensor([new_labels])
            new_total_labels.append(new_labels)
        return new_total_labels

    @staticmethod
    def _add_padding(tokens, labels, max_len, pad_token=0):
        # max_len = max(elem.shape[-1] for elem in tokens)
        for idx, token in enumerate(tokens):
            diff = max_len - token.shape[-1]
            if diff < 0:
                token = token[:, :max_len]
                tokens[idx] = token
                label = labels[idx]
                label = label[:, :max_len]
                labels[idx] = label
            else:
                padding = torch.ones((1, diff), dtype=torch.long) * pad_token
                token = torch.cat([token, padding], dim=1)
                label = labels[idx]
                label = torch.cat([label, padding], dim=1)
                tokens[idx] = token
                labels[idx] = label
        return tokens, labels

    def encode(self, path, pickle_file, max_len=512, pad_token=0):
        # make tokenizer
        self.logger.debug("Creating BertTokenizer")
        tokenizer = BertTokenizer.from_pretrained('nlpaueb/bert-base-greek-uncased-v1')
        total_tokens, total_labels = [], []
        for instance in self.instances:
            tokens = instance[0]
            labels = instance[1]
            self.logger.debug("Processing tokens {} and labels {}".format(tokens, labels))
            new_tokens = []
            new_labels = []
            for word, label in zip(tokens, labels):
                self.logger.debug("Processing word {} and label {}".format(word, label))
                tokenized = tokenizer(word, add_special_tokens=False)["input_ids"]
                new_tokens.extend(tokenized)
                new_labels.extend([label] * len(tokenized))
                self.logger.debug("New tokens {} and labels {}".format(new_tokens, new_labels))
            new_tokens = torch.LongTensor([new_tokens])
            new_labels = torch.LongTensor([new_labels])
            total_tokens.append(new_tokens)
            total_labels.append(new_labels)
        total_tokens, total_labels = self._add_padding(tokens=total_tokens, labels=total_labels, max_len=max_len,
                                                       pad_token=pad_token)
        # total_labels = self._labels_to_one_hot(total_labels=total_labels)
        output = (total_tokens, total_labels, self.encoded_labels)
        with open(join(path, pickle_file), "wb") as f:
            pickle.dump(obj=output, file=f)
        return output
