class Relation:
    id = ""
    segment1 = None
    segment2 = None
    rel = None

    def __init__(self, relation_id, relation, segment1=None, segment2=None):
        self.id = relation_id
        self.segment1 = segment1
        self.segment2 = segment2
        self.rel = relation
