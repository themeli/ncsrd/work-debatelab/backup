import hashlib
import json
import pickle
from os import listdir, mkdir
from os.path import exists, join

import pandas as pd

from backup.data import Document


class DataPreprocessor:
    data = {}
    properties = {}
    documents = []
    logger = None
    repl_char = "="

    def __init__(self, data_path, processed_data_path, properties, logger):
        self.properties = properties
        self.logger = logger
        if properties.get("repl_char"):
            self.repl_char = properties["repl_char"]
        logger.debug("Using replacement character: {}".format(self.repl_char))
        if exists(processed_data_path) and listdir(processed_data_path):
            self.logger.info("Json data files exist! Loading data from json files...")
            files = listdir(processed_data_path)
            for file in files:
                filename = file.split(".")[0]
                with open(join(processed_data_path, file), "r") as f:
                    self.data[filename] = json.loads(f.read())
        else:
            self.logger.info("Json data do not exist. Creating json files from raw data")
            self._preprocess_raw_data(data_path=data_path, processed_data_path=processed_data_path)

    def bio_tagging(self, resources_path, pickle_filename):
        self.logger.info("Start BIO-tagging process")
        for filename, values in self.data.items():
            self.logger.debug("Processing file: {}".format(filename))
            self.logger.debug("========================================================")
            document = Document(filename=filename, logger=self.logger)
            document.preprocess_document(data=values, repl_char=self.repl_char)
            self.documents.append(document)
        self.logger.info("Saving documents into pickle format")
        pickle_file_path = join(resources_path, pickle_filename)
        with open(pickle_file_path, "wb") as f:
            pickle.dump(obj=self.documents, file=f)

    def _preprocess_raw_data(self, data_path, processed_data_path):
        if exists(data_path):
            files = listdir(data_path)
            if files:
                for file in files:
                    if file.endswith(".txt") or file.endswith(".ann"):
                        filename = file.split(".")[0]
                        self.logger.debug("Processing file with name={}".format(filename))
                        # if filename does not already exists in the processed_files dict
                        # add fields
                        if filename not in self.data.keys():
                            hash_id = hashlib.md5(filename.encode())
                            self.data[filename] = {"id": hash_id.hexdigest(), "link": "", "description": "",
                                                   "date": "", "tags": [], "publishedAt": "", "crawledAt": "",
                                                   "domain": "", "netloc": ""}
                        # add content and title
                        if file.endswith(".txt"):
                            self.logger.debug("Adding file content to data dictionary")
                            with open(join(data_path, file), "r") as f:
                                lines = f.readlines()
                                lines = [line for line in lines if line and line != "\n"]
                                self.data[filename]["content"] = " ".join(lines)
                        elif file.endswith(".ann"):
                            self.logger.debug("Processing annotations")
                            df = pd.read_csv(join(data_path, file), index_col=None, header=None, sep="\t")
                            annotations = self.process_df(df)
                            self.data[filename]["annotations"] = annotations
                        json_filename = "{}.json".format(filename)
                        if not exists(processed_data_path):
                            mkdir(processed_data_path)
                        filepath = join(processed_data_path, json_filename)
                        self.logger.debug("Saving json file...")
                        with open(filepath, "w") as f:
                            f.write(json.dumps(self.data[filename], indent=4, sort_keys=False, ensure_ascii=False))

    @staticmethod
    def process_df(df):
        annotations = {"ADUs": [], "Relations": []}
        for index, row in df.iterrows():
            if row[0].startswith("T"):
                adu_type = row[1].split(" ")
                adu = {"id": row[0], "type": adu_type[0], "starts": adu_type[1], "ends": adu_type[2], "segment": row[2]}
                annotations["ADUs"].append(adu)
            elif row[0].startswith("R"):
                rel_type = row[1].split(" ")
                annotations["Relations"].append({"id": row[0], "type": rel_type[0], "arg1": rel_type[1].split(":")[1],
                                                 "arg2": rel_type[2].split(":")[1]})
            else:  # stance
                rel = row[1].split(" ")
                if annotations["ADUs"]:
                    for adu in annotations["ADUs"]:
                        if adu["id"] == rel[1]:
                            if "stance" not in adu.keys():
                                adu["stance"] = []
                            adu["stance"].append({"id": row[0], "type": rel[2]})
        return annotations
