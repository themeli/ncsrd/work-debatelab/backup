import hashlib
import json
from os import getcwd, listdir, mkdir
from os.path import join, exists
from time import time
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

english_folder = "argumentative_essays"
greek_folder = "translations"


def process_df(df):
    data = {"ADUs": [], "Relations": []}
    for index, row in df.iterrows():
        if row[0].startswith("T"):
            adu_type = row[1].split(" ")
            adu = {"id": row[0], "type": adu_type[0], "starts": adu_type[1], "ends": adu_type[2], "segment": row[2]}
            data["ADUs"].append(adu)
        elif row[0].startswith("R"):
            rel_type = row[1].split(" ")
            data["Relations"].append({"id": row[0], "type": rel_type[0], "arg1": rel_type[1].split(":")[1],
                                      "arg2": rel_type[2].split(":")[1]})
        else:  # stance
            rel = row[1].split(" ")
            if data["ADUs"]:
                for adu in data["ADUs"]:
                    if adu["id"] == rel[1]:
                        if "stance" not in adu.keys():
                            adu["stance"] = []
                        adu["stance"].append({"id": row[0], "type": rel[2]})
    return data


def get_json(path):
    files = listdir(path)
    processed_files = {}
    if files:
        for file in files:
            if file.endswith(".txt") or file.endswith(".ann"):
                filename = file.split(".")[0]
                print("Filename={}".format(filename))
                # if filename does not already exists in the processed_files dict
                # add fields
                if filename not in processed_files.keys():
                    hash_id = hashlib.md5(filename.encode())
                    processed_files[filename] = {"id": hash_id.hexdigest(), "link": "", "description": "", "date": "",
                                                 "tags": [], "publishedAt": "", "crawledAt": "", "domain": "",
                                                 "netloc": ""}
                # add content and title
                if file.endswith(".txt"):
                    with open(join(path, file), "r") as f:
                        lines = f.readlines()
                        lines = [line for line in lines if line and line != "\n"]
                    processed_files[filename]["content"] = " ".join(lines)
                elif file.endswith(".ann"):
                    df = pd.read_csv(join(path, file), index_col=None, header=None, sep="\t")
                    data = process_df(df)
                    processed_files[filename]["annotations"] = data
                    processed_files[filename]["annotations"]["document_link"] = ""
                json_filename = "{}.json".format(filename)
                filepath = join(getcwd(), "output", json_filename)
                with open(filepath, "w") as f:
                    f.write(json.dumps(processed_files[filename], indent=4, sort_keys=False))
    return processed_files


def get_list_contents(files):
    texts = []
    filenames = []
    for filename, data in files.items():
        texts.append(data["content"])
        filenames.append(filename)
    return texts, filenames


def print_top_words(model, feature_names, n_top_words):
    messages = []
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message_list = [feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]
        messages.append(message_list)
        message += " ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        print(message)
    return messages


def perform_lda(data, filenames, initial_data, language):
    n_samples = "all"
    n_features = 1000
    n_components = 400
    n_top_words = 5

    t0 = time()
    data_samples = data if n_samples == "all" else data[:n_samples]
    print("done in %0.3fs." % (time() - t0))
    stopwords_list = stopwords.words("greek") if language == languages[1] else stopwords.words("english")
    # Use tf-idf features for NMF.
    print("Extracting tf-idf features for NMF...")
    # tfidf_vectorizer = TfidfVectorizer(max_df=0.95, min_df=2,
    #                                    max_features=n_features,
    #                                    stop_words=stopwords_list)
    t0 = time()
    print("done in %0.3fs." % (time() - t0))

    # Use tf (raw term count) features for LDA.
    print("Extracting tf features for LDA...")
    tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2,
                                    max_features=n_features,
                                    stop_words=stopwords_list)
    t0 = time()
    tf = tf_vectorizer.fit_transform(data_samples)
    print("done in %0.3fs." % (time() - t0))
    print()
    lda = LatentDirichletAllocation(n_components=n_components, max_iter=5,
                                    learning_method='online',
                                    learning_offset=50.,
                                    random_state=0)
    t0 = time()
    predictions = lda.fit_transform(tf)
    predictions = np.argmax(predictions, axis=1)
    print("done in %0.3fs." % (time() - t0))

    print("\nTopics in LDA model:")
    tf_feature_names = tf_vectorizer.get_feature_names()
    topics = print_top_words(lda, tf_feature_names, n_top_words)

    for idx, prediction in enumerate(predictions):
        doc_topics = topics[prediction]
        filename = filenames[idx]
        doc_data = initial_data[filename]
        doc_data["annotations"]["topics"] = doc_topics
        with open(join(getcwd(), "output", filename + ".json"), "w") as f:
            json.dump(doc_data, f, indent=4, sort_keys=False, ensure_ascii=False)


def main(language):
    if not exists(join(getcwd(), "output")):
        mkdir(join(getcwd(), "output"))
    english_path = join(getcwd(), "data", english_folder)
    greek_path = join(getcwd(), "data", greek_folder)
    processed_files = get_json(english_path) if language == languages[0] else get_json(greek_path)
    contents, filenames = get_list_contents(processed_files)
    perform_lda(contents, filenames, processed_files, language)


if __name__ == '__main__':
    languages = ["eng", "gr"]
    lang = languages[1]
    main(language=lang)
