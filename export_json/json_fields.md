# JSON Documentation

* id: the document ID in the ElasticSearch
* link: the link to the site where the document is retrieved
* date: entry creation timestamp
* description: a short description of the document
* title: the title of the article
* content: the content of the article
* publishedAt: date of publication
* crawledAt: date of retrieval
* domain: the site domain (e.g. "in" for www.in.gr)
* netloc: the base url of the site (e.g. www.in.gr)
* tags: a list of the tags for the document
* annotations: dictionary with the ADUs and their relationship as well as a link to the document in the ElasticSearch
    - ADUs: list of the ADUs in the document. Each ADU contains an ID, the segment, the type (e.g. Major Claim, Claim or Premise), the starting and ending characters and the stance
    - Relations: list of ADU relations which contains an ID, the type (support, attacks) and the source and origin arguments
    - topics: keyword / topics related to the specific article