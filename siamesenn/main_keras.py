import json
import logging
import random
from datetime import datetime

import numpy as np
import pandas as pd
import yaml
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

from siamesenn_keras import *


def config_logger():
    """
    Creates a logger which writes both in a file and in the console

    Returns
        Logger: the logger object
    """
    if not exists(join(getcwd(), "output")):
        mkdir(join(getcwd(), "output"))
    run_dir = "run_%s" % datetime.now().strftime('%Y%m%d-%H%M%S')
    mkdir(join(getcwd(), "output", run_dir))
    mkdir(join(getcwd(), "output", run_dir, "logs"))
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    program_logger = logging.getLogger(__name__)

    program_logger.setLevel(logging.INFO)
    log_filename = 'logs_%s' % datetime.now().strftime('%Y%m%d-%H%M%S')
    run_dir_path = join(getcwd(), "output", run_dir, "logs")
    file_handler = logging.FileHandler("{0}/{1}.log".format(run_dir_path, log_filename))
    file_handler.setFormatter(log_formatter)
    program_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    program_logger.addHandler(console_handler)
    return program_logger, run_dir, log_filename


def load_properties():
    """
    Reads the properties file to load the configuration

    Returns
        | dict: a dictionary containing the project's configuration
    """

    resources_folder = join(getcwd(), "resources")
    properties_file = "properties.yaml" if exists(
        join(resources_folder, "properties.yaml")) else "example_properties.yaml"
    properties_path = join(getcwd(), "resources", properties_file)
    with open(properties_path, 'r') as f:
        return yaml.safe_load(f)


def read_data(logger, properties):
    """
    Reads the data files from resources/data folder. Based on the configuration, for the training it uses either the
    dev or the train file

    Args
        | properties: dict with the project's configuration

    Returns
        | tuple: the two DataFrame objects with the train and test data
    """
    folder = properties["dataset"]["folder"]
    test_file = "test.csv"
    train_file = "{}.csv".format(properties["dataset"]["file"])
    data_folder = join(getcwd(), "resources", "data", folder)
    logger.info("Reading data")
    train_data = pd.read_csv(join(data_folder, train_file), index_col=None, header=None, sep="\t")
    if properties["dataset"]["file"] == "train":
        dev_data = pd.read_csv(join(data_folder, "dev.csv"), index_col=None, header=None, sep="\t")
        frames = [train_data, dev_data]
        train_data = pd.concat(frames)
    test_data = pd.read_csv(join(data_folder, test_file), index_col=None, header=None, sep="\t")
    return train_data, test_data


def labels_to_numerical(df):
    positives = df[1] == "Y"
    df.loc[positives, 1] = 1
    df.loc[~positives, 1] = 0
    return df


def get_data_and_labels(df, trainset=True):
    vectors = []
    labels = []
    input_dim = None
    for index, row in df.iterrows():
        vector = row[2].split(" ")
        vector = [float(f) for f in vector]
        if index == 0 and trainset:
            input_dim = len(vector)
        vectors.append(vector)
        label = row[1]
        labels.append(label)
    return np.asarray(vectors), np.asarray(labels), input_dim


def get_siamese_data(data, labels):
    data = list(data)
    labels = list(labels)

    anchors = data
    positives = []
    negatives = []

    for i, dat in enumerate(data):
        curr_label = labels[i]
        pos_idx = [labels.index(labels[j]) for j in range(len(labels)) if labels[j] == curr_label and i != j]
        neg_idx = [labels.index(labels[j]) for j in range(len(labels)) if labels[j] != curr_label]
        pos = random.choice(pos_idx)
        neg = random.choice(neg_idx)
        positives.append(data[pos])
        negatives.append(data[neg])
    anchors = np.asarray(anchors)
    positives = np.asarray(positives)
    negatives = np.asarray(negatives)
    labels = np.asarray(labels)
    return anchors, positives, negatives, labels


def performance(logger, true_labels, predicted_labels):
    logger.info("Accuracy: {}".format(accuracy_score(y_true=true_labels, y_pred=predicted_labels)))
    logger.info(
        "Macro Precision: {}".format(precision_score(y_true=true_labels, y_pred=predicted_labels, average="macro")))
    logger.info(
        "Micro Precision: {}".format(precision_score(y_true=true_labels, y_pred=predicted_labels, average="micro")))
    logger.info("Macro Recall: {}".format(recall_score(y_true=true_labels, y_pred=predicted_labels, average="macro")))
    logger.info("Micro Recall: {}".format(recall_score(y_true=true_labels, y_pred=predicted_labels, average="micro")))
    logger.info("Macro F: {}".format(f1_score(y_true=true_labels, y_pred=predicted_labels, average="macro")))
    logger.info("Micro F: {}".format(f1_score(y_true=true_labels, y_pred=predicted_labels, average="micro")))


def predictions_to_file(true_labels, predicted_labels, run_dir, filename="eval_keras_mlp.csv"):
    df = pd.DataFrame({"labels": true_labels, "predictions": predicted_labels})
    run_dir_path = join(getcwd(), "output", run_dir)
    file = join(run_dir_path, filename)
    df.to_csv(file, sep=",", encoding="UTF-8")


def save_siamese_embeddings(train_df, test_df, train_out, test_out, folder):
    train_list = []
    for vector in train_out:
        vector = list(vector)
        str_vector = " ".join([str(i) for i in vector])
        train_list.append(str_vector)
    test_list = []
    for vector in test_out:
        vector = list(vector)
        str_vector = " ".join([str(i) for i in vector])
        test_list.append(str_vector)
    train_df[3] = train_list
    train_file = join(getcwd(), "resources", "data", folder, "train_new.csv")
    train_df.to_csv(train_file, sep="\t", encoding="UTF-8", index=None, header=None)

    test_file = join(getcwd(), "resources", "data", folder, "test_new.csv")
    test_df[3] = test_list
    test_df.to_csv(test_file, sep="\t", encoding="UTF-8", index=None, header=None)


def main():
    # configure logger & load properties
    logger, run_dir, log_filename = config_logger()
    props = load_properties()
    logger.info("Project's configuration: {}".format(json.dumps(props, sort_keys=True, indent=4)))
    logger.info("Configuration file is loaded!")

    # Load train/test datasets
    train_df, test_df = read_data(logger=logger, properties=props)
    train_df = labels_to_numerical(train_df)
    test_df = labels_to_numerical(test_df)
    train_data, train_labels, input_dim = get_data_and_labels(train_df)
    test_data, test_labels, _ = get_data_and_labels(test_df)
    logger.info("Running simple classification with Keras Sequential model")
    # classifier = ClassificationNN(properties=props, input_dim=input_dim)
    # classifier.train(train_data=train_data, train_labels=train_labels, properties=props, run_dir=run_dir,
    #                  log_filename=log_filename)
    # true_labels, predicted_labels = classifier.test(test_data=test_data, test_labels=test_labels, properties=props)
    #
    # performance(logger=logger, true_labels=true_labels, predicted_labels=predicted_labels)
    # predictions_to_file(true_labels=true_labels, predicted_labels=predicted_labels, run_dir=run_dir)

    if props["model"] == "siamese":
        logger.info("Running Siamese Neural Network")
        train_anchors, train_positives, train_negatives, train_labels = get_siamese_data(train_data, train_labels)
        test_anchors, test_positives, test_negatives, test_labels = get_siamese_data(test_data, test_labels)

        train_data = {"anchors": train_anchors, "positives": train_positives, "negatives": train_negatives,
                      "labels": train_labels}
        test_data = {"anchors": test_anchors, "positives": test_positives, "negatives": test_negatives,
                     "labels": test_labels}
        classifier = BertSiameseNN(properties=props, input_dim=input_dim, logger=logger)
        classifier.train(train_data=train_data, properties=props, run_dir=run_dir, log_filename=log_filename)
        true_labels, predicted_labels = classifier.test(test_data=test_data)
        performance(logger=logger, true_labels=true_labels, predicted_labels=predicted_labels)
        predictions_to_file(true_labels=true_labels, predicted_labels=predicted_labels, run_dir=run_dir,
                            filename="eval_keras_siamese.csv")
        train_out = classifier.get_embeddings(train_data)
        test_out = classifier.get_embeddings(test_data)
        save_siamese_embeddings(train_df, test_df, list(train_out), list(test_out), props["dataset"]["folder"])


if __name__ == '__main__':
    main()
