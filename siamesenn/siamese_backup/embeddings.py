import csv
import pickle
import random
import sys
from itertools import groupby
from os import getcwd
from os.path import join

import numpy as np
import pandas as pd
import torch
from tqdm import tqdm


class Embeddings:

    def __init__(self, kind, logger):
        self.embeddings_kind = kind
        self.logger = logger
        self.train_segments = []
        self.test_segments = []
        self.train_siamese_data = []
        self.test_siamese_data = []
        self.train_words = []
        self.test_words = []
        self.baseline_train_data = []
        self.baseline_train_labels = []
        self.baseline_test_data = []
        self.baseline_test_labels = []

    def get_cache_name(self):
        return f"cached_kind{self.embeddings_kind}.pkl"

    def make_siamese_input_data(self, segments):
        """
        Function to generate the data for the Siamese Neural Network. Each entry contains a positive instance with its
        label as well as a negative instance.

        Args
            | segments (list): a list of the created segments

        Returns
            | list: the data for the Siamese Neural Network
        """
        # if dealing with > 2 classes ==> stratification

        self.logger.info("Making pos / neg indices")
        pos_idxs = [i for i in range(len(segments)) if segments[i]["label"] == 1]
        neg_idxs = [i for i in range(len(segments)) if segments[i]["label"] == 0]
        random.shuffle(pos_idxs)
        random.shuffle(neg_idxs)
        # pad
        sz_diff = len(segments) - len(pos_idxs)
        while sz_diff > 0:
            pos_idxs += pos_idxs[:sz_diff]
            random.shuffle(pos_idxs)
            sz_diff = len(segments) - len(pos_idxs)
        sz_diff = len(segments) - len(neg_idxs)
        while sz_diff > 0:
            neg_idxs += neg_idxs[:sz_diff]
            random.shuffle(neg_idxs)
            sz_diff = len(segments) - len(neg_idxs)

        siamese_data = []
        self.logger.info("Bulding anchors, pos, neg")
        for seg_idx, seg in enumerate(tqdm(segments, total=len(segments))):
            lbl = seg["label"]
            # assign the vector that matches the label
            obj = {"label": lbl, "anchor": seg["vector"]}

            pos_src, neg_src = (pos_idxs, neg_idxs) if lbl == 1 else (neg_idxs, pos_idxs)

            pos_idx = pos_src.pop(0)
            while pos_idx == seg_idx:
                pos_src.append(pos_idx)
                pos_idx = pos_src.pop(0)
            obj["positive"] = segments[pos_idx]["vector"]
            obj["negative"] = segments[neg_src.pop(0)]["vector"]
            siamese_data.append(obj)
        return siamese_data

    def preprocess_data(self, train_data, test_data, properties, logger, debug_limit=None):
        """
        Preprocessing of the training and testing data. The sentences are split into segments based on the labels inside
        the sentence.

        Args
            | train_data (DataFrame): pandas DataFrame containing the training data
            | test_data (DataFrame): pandas DataFrame containing the test data
            | properties (dict): dictionary with the project's configuration

        """
        if debug_limit:
            train_data = train_data.iloc[:debug_limit, :]
            test_data = test_data.iloc[:debug_limit, :]
        logger.info("Preprocessing train")
        self.train_segments, self.train_words = self.preprocess_df(train_data)
        self.train_siamese_data = self.make_siamese_input_data(self.train_segments)
        logger.info("Training data transformed to Siamese dataset")
        logger.info("Preprocessing test")
        self.test_segments, self.test_words = self.preprocess_df(test_data)
        self.logger.info("Making siamese inputs")
        self.test_siamese_data = self.make_siamese_input_data(self.test_segments)
        logger.info("Test data transformed to siamese dataset")
        if properties["embeddings"]["kind"] == "glove":
            self.prepare_baseline_data(self.train_segments, self.test_segments)
        elif properties["embeddings"]["kind"] == "bert":
            self.prepare_baseline_data(train_data, test_data)
        with open(join(getcwd(), "../output", self.get_cache_name()), "wb") as f:
            logger.info("Saving datasets into pickle file. Creating cache...")
            pickle.dump([self.train_siamese_data, self.test_siamese_data, self.train_segments, self.test_segments], f)
            logger.info("Datasets saved!")

    def preprocess_df(self, data):
        raise NotImplementedError

    def prepare_baseline_data(self, train, test):
        raise NotImplementedError


class GloveEmebddings(Embeddings):

    def __init__(self, kind, logger, maxlen, glove_filename):
        super(GloveEmebddings, self).__init__(kind, logger)
        self.maxlen = maxlen
        self.glove_embeddings = self.load_glove_file(glove_filename)

    @staticmethod
    def _text_to_glove(glove_df, word_list):
        """
        Takes the pre-processed text created by the preprocess_text function and converts it to a vector of numbers
        using the word embeddings from Glove (https://nlp.stanford.edu/projects/glove/).
        This process is being done for every word of the text separately. In the end all the vectors
        are gathered in a list of vectors (embeddings)

        Args
            glove_df (DataFrame): the file with the Glove embeddings
            word_list (list): segment's words list

        Returns
            ndarray: a vector for every segment
        """
        embeddings = []
        for word in word_list:
            # to lowercase
            word = word.lower()
            if word not in glove_df.index:
                continue
            embeddings.append(glove_df.index.get_loc(word))
        embeddings = torch.LongTensor(embeddings)
        return embeddings

    @staticmethod
    def load_glove_file(glove_filename):
        """
        Creates the glove's file path and reads this file.

        Args:
            glove_filename (str): the name of the GloVe file from the configuration file
        Returns:
            DataFrame: glove word indexes
        """
        max_int = sys.maxsize
        glove_file_path = join(getcwd(), "../resources", glove_filename)
        while True:
            # decrease the maxInt value by factor 10
            # as long as the OverflowError occurs.
            try:
                csv.field_size_limit(max_int)
                break
            except OverflowError:
                max_int = int(max_int / 10)
        res = pd.read_csv(glove_file_path, index_col=0, delimiter=" ", quoting=3, header=None, engine="python",
                          error_bad_lines=False)
        res = res.astype(np.float32)
        return res

    def preprocess_df(self, data):
        segments = []
        words = []
        # format sentences
        data[5] = data[5].apply(lambda x: int(x.split(":")[1].strip()))
        # make binary numeric labels
        positives = data[2] != "O"
        data.loc[positives, 2] = 1
        data.loc[~positives, 2] = 0
        # group by sentences and keep only words and labels
        df = data.groupby([5])[[0, 2]].apply(lambda x: x.values.tolist())

        # iterate sentences
        for sentence in tqdm(df):
            # group sentence segments by consecutive label values (keys)
            # e.g. AAAAABBBBBCCCDAA ---> (AAAAA, A), (BBBB, B),... C D A
            # expand generators and keep only the words in the groupped object(s)
            groupped_data = [(k, [wk[0] for wk in g]) for k, g in groupby(sentence, key=lambda x: x[-1])]
            for sentence_label, segment_words in groupped_data:
                vec = self._text_to_glove(self.glove_embeddings, segment_words)

                if len(vec) > self.maxlen:
                    vec = vec[:self.maxlen]
                elif len(vec) < self.maxlen:
                    pad = torch.zeros((self.maxlen,), dtype=vec.dtype)
                    pad[:len(vec)] = vec
                    vec = pad
                segments.append({"vector": vec, "label": sentence_label})
                words.append(segment_words)
        return segments, words

    def prepare_baseline_data(self, train, test):
        for train_segment in train:
            self.baseline_train_data.append(train_segment["vector"])
            self.baseline_train_labels.append(train_segment["label"])
        for test_segment in test:
            self.baseline_test_data.append(test_segment["vector"])
            self.baseline_test_labels.append(test_segment["label"])


class BertEmbeddings(Embeddings):

    def __init__(self, kind, logger, train_data):
        super(BertEmbeddings, self).__init__(kind, logger)
        self.train_data = train_data

    def get_embeddings(self):
        # get vectors
        ddf = self.train_data[2].apply(lambda x: np.asarray([float(k) for k in x.split()]))
        return np.vstack(ddf.values)

    def preprocess_df(self, data):
        segments = []
        words = []
        positives = data[1] == "Y"
        data.loc[positives, 1] = 1
        data.loc[~positives, 1] = 0
        vectors = []
        with tqdm(total=len(data)) as pbar:
            for index, row in data.iterrows():
                words.append(row[0])
                vector = row[2].split(" ")
                vector = [float(f) for f in vector]
                vectors.append(vector)
                vec = torch.FloatTensor(vector)
                label = row[1]
                segments.append({"vector": vec, "label": label})
                pbar.update()
        return segments, words

        # return just the index and the label
        # for index, row in data.iterrows():
        #     label = row[1]
        #     segments.append({"vector": index, "label": label})
        # return segments, words

    def prepare_baseline_data(self, train_df, test_df):
        self.baseline_train_data = train_df[2].apply(lambda x: np.asarray([float(k) for k in x.split()]))
        self.baseline_test_data = test_df[2].apply(lambda x: np.asarray([float(k) for k in x.split()]))
        self.baseline_train_labels = np.asarray(list(train_df[1]))
        self.baseline_test_labels = np.asarray(list(test_df[1]))
