from os import getcwd
from os.path import join

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as functional
from sklearn.metrics import accuracy_score
from torch.utils.data import DataLoader, Dataset
from collections import Counter

from torch.utils.tensorboard import SummaryWriter


def minmax(inp):
    if type(inp) == torch.Tensor:
        mmin, mmax = inp.min().item(), inp.max().item()
    else:
        mmin, mmax = min(inp), max(inp)
    return "{:.3f}, {:.3f}".format(mmin, mmax)


class SiameseDataset(Dataset):
    """
    Class extending the torch Dataset class. Contains the data and their labels.
    Functions __len__ and __getitem__ should be overridden.
    """
    # TorchDataset
    data = []
    labels = []

    def __init__(self, data):
        super(SiameseDataset, self).__init__()
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]


class ContrastiveLoss(nn.Module):
    """
    Custom class representing the triplet loss of the Siamese Network
    """

    def __init__(self, logger, device):
        super(ContrastiveLoss, self).__init__()
        self.pdist = nn.PairwiseDistance(2.0)
        self.logger = logger
        self.device = device

    def compute(self, output1, output2, label, margin=2):
        """
        Function to compute the constrastive loss

        Args
            | output1: the output of the positive input in the Siamese Network
            | output2: the output of the negative input in the Siamese Network
            | label: the label of the positive input

        Returns
            | the triplet loss
        """
        # see: https://discuss.pytorch.org/t/batched-pairwise-distance/39611
        # Find the pairwise distance or eucledian distance of two output feature vectors
        euclidean_distance = functional.pairwise_distance(output1.to(self.device), output2.to(self.device))
        loss_contrastive = torch.mean((1 - label.to(self.device)) * torch.pow(euclidean_distance.to(self.device), 2) +
                                      label.to(self.device) * torch.pow(
            torch.clamp(margin - euclidean_distance.to(self.device), min=0.0), 2))
        self.logger.debug("Distance: {}".format(euclidean_distance))
        return loss_contrastive


class SiameseNN(nn.Module):

    def __init__(self, logger, kind, embeddings=None):
        super(SiameseNN, self).__init__()
        self.device = self.get_device()
        self.logger = logger
        self.kind = kind
        if embeddings is not None:
            self.input_dim = embeddings.shape[-1]
            embeddings = torch.Tensor(embeddings)
            self.embedding_layer = torch.nn.Embedding.from_pretrained(embeddings).to(self.device)

    def make_binary_dense(self):
        self.binary_dense = nn.Linear(self.hidden, 2)

    @staticmethod
    def get_device():
        return torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")

    def forward(self, inp_indices):
        raise NotImplementedError

    def train_siamese(self, train_data, properties):
        """
            Function for the Siamese Network training

            Args
                | train_data (list): the data produced for the Siamese Network
                | embeddings(df): the initial embeddings
                | properties (dict): the dictionary with the project's configuration

            Returns
                | the trained model
        """
        self.logger.info("Start training the Siamese NN")
        trainset = SiameseDataset(train_data)
        trainloader = torch.utils.data.DataLoader(trainset, batch_size=properties["siamese"]["batch"],
                                                  shuffle=properties["siamese"]["shuffle-train"],
                                                  num_workers=2)

        # criterion = ContrastiveLoss(self.logger, self.device)
        if properties["siamese"]["freeze-embeddings"]:
            self.embedding_layer.freeze = True

        writer = SummaryWriter()

        # euclidean distance triplet loss
        margin = 1
        criterion = torch.nn.TripletMarginLoss(p=2, margin=margin)
        lr = properties["siamese"]["learning_rate"]
        # optimizer = torch.optim.Adam(self.parameters(), lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0,
        #                              amsgrad=False)
        optimizer = torch.optim.SGD(self.parameters(), lr=lr)
        global_iter = 0
        cumulative_accuracy = []
        for epoch in range(properties["siamese"]["epochs"]):  # loop over the dataset multiple times
            epoch_loss = []
            for i, data in enumerate(trainloader):
                optimizer.zero_grad()
                # get the inputs; data is a list of [inputs, labels]
                labels = data["label"]

                # preds, loss = self.forward_and_loss_siamese(data)
                preds, loss = self.forward_and_loss_classification(data)

                # do the backprop
                loss.backward()
                loss = loss.item()
                epoch_loss.append(loss)
                optimizer.step()

                writer.add_scalar('Loss/train', loss, global_iter)
                cumulative_accuracy.append(self.get_batch_accuracy(preds, labels))

                writer.add_scalar('Accuracy/test', sum(cumulative_accuracy) / len(cumulative_accuracy), global_iter)
                global_iter += 1
            print(f"Epoch: {epoch}, total loss: {sum(epoch_loss)} mean loss {sum(epoch_loss) / len(epoch_loss)}")

        self.logger.info('Finished Training')
        # import ipdb; ipdb.set_trace()

    def out_minmax(self, msg, tensors):
        res = ""
        for t in tensors:
            res += f"{minmax(t)}"
        self.logger.info(f"{msg} {res}")

    @staticmethod
    def get_batch_accuracy(preds, labels):
        return torch.eq(torch.LongTensor(preds), labels).sum().item()

    def get_batch_predictions(self, anchor_outputs, pos_outputs, neg_outputs):
        # compute distances wrt. the anchor
        dist_pos = functional.pairwise_distance(anchor_outputs.to(self.device), pos_outputs.to(self.device))
        dist_neg = functional.pairwise_distance(anchor_outputs.to(self.device), neg_outputs.to(self.device))
        # group
        dists = torch.stack([dist_pos, dist_neg], axis=1)
        # get argmins
        predictions = torch.argmin(dists, axis=1).cpu().tolist()
        return predictions

    def forward_and_loss_classification(self, data):
        positives, negatives, anchors, labels = data['positive'], data['negative'], data['anchor'], data[
            'label']
        # positives.to(self.device), negatives.to(self.device),
        anchors.to(self.device)
        labels.to(self.device)
        out_anchors = self.forward(anchors)

        out_binary = self.binary_dense(out_anchors)
        out_softmax = functional.log_softmax(out_binary, dim=1)
        output_loss = functional.nll_loss(out_softmax, labels)
        predictions = torch.argmax(out_softmax, dim=1)
        # loss(predictions, labels)
        return predictions, output_loss

    def forward_and_loss_siamese(self, data, margin=1.0):
        positives, negatives, anchors, labels = data['positive'], data['negative'], data['anchor'], data[
            'label']
        positives.to(self.device), negatives.to(self.device), labels.to(self.device)

        # self.out_minmax("test nn input", [anchors, positives, negatives])
        out_anchors = self.forward(anchors)
        out_pos = self.forward(positives)
        out_neg = self.forward(negatives)
        # self.out_minmax("test nn output", [out_anchors, out_pos, out_neg])

        # siamese losses intuition:
        # https://towardsdatascience.com/how-to-choose-your-loss-when-designing-a-siamese-neural-net-contrastive-triplet-or-quadruplet-ecba11944ec

        # triplet loss
        # loss = criterion(out_anchors, out_pos, out_neg)
        # triplet squared
        loss = torch.mean(
            torch.clamp(
                functional.pairwise_distance(out_anchors, out_pos) -
                functional.pairwise_distance(out_anchors, out_neg) + margin, min=0.0)
        )

        predictions = self.get_batch_predictions(out_anchors, out_pos, out_neg)
        return predictions, loss

    def test_siamese(self, test_data, properties, run_dir):
        """
            Function to test the Siamese Network

            Args
                | net: the trained model
                | test_data (df): the test data produced in the correct format to be fed in the Siamese Network
            """
        self.logger.info("Start testing")
        self.eval()
        testset = SiameseDataset(test_data)
        testloader = torch.utils.data.DataLoader(testset, batch_size=properties["siamese"]["batch"], num_workers=2)
        writer = SummaryWriter()
        global_iter = 0
        cumulative_accuracy = []
        y_true, y_pred = [], []
        with torch.no_grad():
            for i, data in enumerate(testloader, 0):
                labels = data["label"]
                predictions, loss = self.forward_and_loss_classification(data)
                y_true += list(labels.numpy())
                y_pred += list(predictions.numpy())

                cumulative_accuracy.append(self.get_batch_accuracy(predictions, labels))
                writer.add_scalar('Accuracy/test', sum(cumulative_accuracy) / len(cumulative_accuracy), global_iter)
                global_iter += 1

        counts = Counter(y_pred)
        for cl, count in counts.most_common():
            print(f"Class {cl} : {count} ({count / len(y_pred) * 100}) %%")
        accuracy = accuracy_score(y_true=np.asarray(y_true), y_pred=np.asarray(y_pred))
        self.logger.info("Model's accuracy: {}".format(accuracy))

        df = pd.DataFrame({"labels": y_true, "predictions": y_pred})
        run_dir_path = join(getcwd(), "../output", run_dir)
        file = join(run_dir_path, "eval_bert.csv") if self.kind == "bert" else join(run_dir_path, "eval_glove.csv")
        df.to_csv(file, sep=",", encoding="UTF-8")
        self.logger.info("Testing finished!")


class GloveSiameseNN(SiameseNN):

    def __init__(self, logger, kind, embeddings=None, hidden=100, num_layers=2):
        super(GloveSiameseNN, self).__init__(logger, kind, embeddings.values)

        self.hidden = hidden
        self.num_layers = num_layers

        self.lstm = nn.LSTM(input_size=self.input_dim, hidden_size=self.hidden, num_layers=self.num_layers,
                            batch_first=True).to(self.device)
        self.make_binary_dense()

    def forward(self, inp_indices):
        vectors = self.embedding_layer(inp_indices.to(self.device)).to(self.device)
        # keep the last timestep
        _, last_hidden_and_cell = self.lstm_forward(vectors)
        last_hidden = last_hidden_and_cell[0]
        # return a single vector for all layers
        last_hidden = torch.reshape(last_hidden, (-1, self.hidden * self.num_layers))
        return last_hidden

    def lstm_forward(self, inp):
        return self.lstm(inp)


class BertSiameseNN(SiameseNN):

    def __init__(self, logger, kind, input_dim, activation_function, dropout, hidden=100, num_layers=2):
        super(BertSiameseNN, self).__init__(logger, kind)
        self.hidden = hidden
        self.num_layers = num_layers
        self.activation_function = activation_function
        self.dropout = dropout

        self.input_dim = input_dim

        layers_size = []
        for i in range(num_layers):
            layers_size.append(hidden)
        self.nn_layers = self.make_linear_chain(input_dim=self.input_dim, dim_list=layers_size)
        self.make_binary_dense()

    @staticmethod
    def make_linear(input_dim, output_dim):
        """Make a linear fully-connected layer
        """
        return nn.Linear(input_dim, output_dim)

    def make_linear_chain(self, input_dim, dim_list):
        """Make a chain of connected linear layers
        Arguments:
            input_dim {int} -- Input dimension
            dim_list {int} -- Dimension of subsequent layers
        Returns:
        """
        layers = []
        current = input_dim
        # make the chain
        for dim in dim_list:
            layers.append(self.make_linear(current, dim))
            current = dim
        # return as a module list
        return nn.ModuleList(layers)

    def run_linear_chain(self, input_data, activation_func=None, dropout_keep_prob=None):
        """Run a chain of connected linear layers
        Arguments:
            input_data {torch.Tensor} -- Input data
            layers_chain {nn.ModuleList} -- List of layers to execute in sequence
            activation_func {function} -- Activation func to apply after each layer
        Returns:
        """
        current_data = input_data

        for layer in self.nn_layers:
            current_data = activation_func(layer(current_data))
            if dropout_keep_prob is not None:
                if not self.training:
                    dropout_keep_prob = 1.0
                current_data = functional.dropout(current_data, p=dropout_keep_prob)
        return current_data

    def forward(self, inp_indices):
        if self.activation_function == "relu":
            act_function = functional.relu
        elif self.activation_function == "sigmoid":
            act_function = torch.sigmoid
        elif self.activation_function == "tanh":
            act_function = torch.tanh
        elif self.activation_function == "leaky_relu":
            act_function = functional.leaky_relu
        else:
            # ReLU activations by default
            act_function = functional.relu
        return self.run_linear_chain(input_data=inp_indices, activation_func=act_function,
                                     dropout_keep_prob=self.dropout)
