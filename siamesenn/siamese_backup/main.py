import json
import logging
import pickle as pkl
from datetime import datetime
from os import getcwd, mkdir
from os.path import join, exists

import numpy as np
import pandas as pd
import yaml
from sklearn.dummy import DummyClassifier
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier

from backup.embeddings import Embeddings, GloveEmebddings, BertEmbeddings
from backup.siamesenn import SiameseNN, GloveSiameseNN, BertSiameseNN


def config_logger():
    """
    Creates a logger which writes both in a file and in the console

    Returns
        Logger: the logger object
    """
    if not exists(join(getcwd(), "../output")):
        mkdir(join(getcwd(), "../output"))
    run_dir = "run_%s" % datetime.now().strftime('%Y%m%d-%H%M%S')
    mkdir(join(getcwd(), "../output", run_dir))
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    program_logger = logging.getLogger(__name__)

    program_logger.setLevel(logging.INFO)
    log_filename = 'logs_%s' % datetime.now().strftime('%Y%m%d-%H%M%S')
    run_dir_path = join(getcwd(), "../output", run_dir)
    file_handler = logging.FileHandler("{0}/{1}.log".format(run_dir_path, log_filename))
    file_handler.setFormatter(log_formatter)
    program_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    program_logger.addHandler(console_handler)
    return program_logger, run_dir, log_filename


def load_properties():
    """
    Reads the properties file to load the configuration

    Returns
        | dict: a dictionary containing the project's configuration
    """

    resources_folder = join(getcwd(), "../resources")
    properties_file = "properties.yaml" if exists(
        join(resources_folder, "properties.yaml")) else "example_properties.yaml"
    properties_path = join(getcwd(), "../resources", properties_file)
    with open(properties_path, 'r') as f:
        return yaml.safe_load(f)


def read_data(logger, properties):
    """
    Reads the data files from resources/data folder. Based on the configuration, for the training it uses either the
    dev or the train file

    Args
        | properties: dict with the project's configuration

    Returns
        | tuple: the two DataFrame objects with the train and test data
    """
    folder = properties["dataset"]["folder"]
    test_file = "test.csv"
    train_file = "{}.csv".format(properties["dataset"]["file"])
    data_folder = join(getcwd(), "../resources", "data", folder)
    logger.info("Reading data")
    train_data = pd.read_csv(join(data_folder, train_file), index_col=None, header=None, sep="\t")
    test_data = pd.read_csv(join(data_folder, test_file), index_col=None, header=None, sep="\t")
    return train_data, test_data


def get_embeddings_and_model(logger, properties, train_data, test_data):
    kind = properties["embeddings"]["kind"]
    hidden = properties["siamese"]["hidden"]
    num_layers = properties["siamese"]["num_layers"]
    if kind == "glove":
        maxlen = properties["maxlen"]
        glove_filename = properties["embeddings"]["filename"]
        embeddings = GloveEmebddings(kind=kind, logger=logger, maxlen=maxlen, glove_filename=glove_filename)
        model = GloveSiameseNN(logger=logger, kind=kind, embeddings=embeddings.glove_embeddings, hidden=hidden,
                               num_layers=num_layers)
    elif kind == "bert":
        embeddings = BertEmbeddings(kind=kind, logger=logger, train_data=train_data)
        input_dim = len(train_data[2][0].split(" "))
        activation_function = properties["siamese"]["activation_function"]
        dropout = properties["siamese"]["dropout"]
        model = BertSiameseNN(logger=logger, kind=kind, input_dim=input_dim, dropout=dropout,
                              hidden=hidden,
                              num_layers=num_layers, activation_function=activation_function)
    else:
        embeddings = Embeddings(kind=kind, logger=logger)
        model = SiameseNN(logger=logger, kind=kind, embeddings=None)

    pickle_location = join(getcwd(), "../output", embeddings.get_cache_name())
    if exists(pickle_location):
        with open(pickle_location, "rb") as f:
            embeddings.train_siamese_data, embeddings.test_siamese_data, embeddings.train_segments, \
                embeddings.test_segments = pkl.loads(f.read())
        if kind == "glove":
            embeddings.prepare_baseline_data(embeddings.train_segments, embeddings.test_segments)
        elif kind == "bert":
            embeddings.prepare_baseline_data(train_data, test_data)
    else:
        embeddings.preprocess_data(train_data=train_data, test_data=test_data, properties=properties, logger=logger)
    return embeddings, model


def baseline(embeddings, logger, run_dir):
    """Compute the baseline classification """
    train_unique, train_counts = np.unique(embeddings.baseline_train_labels, return_counts=True)
    train_instances = dict(zip(train_unique, train_counts))
    test_unique, test_count = np.unique(embeddings.baseline_test_labels, return_counts=True)
    test_instances = dict(zip(test_unique, test_count))
    logger.info("Training instances {}".format(train_instances))
    logger.info("Test instances {}".format(test_instances))

    embeddings.baseline_train_data = np.asarray(list(embeddings.baseline_train_data))
    embeddings.baseline_test_data = np.asarray(list(embeddings.baseline_test_data))

    embeddings.baseline_train_labels[embeddings.baseline_train_labels == "Y"] = 1
    embeddings.baseline_train_labels[embeddings.baseline_train_labels == "N"] = 0
    embeddings.baseline_train_labels = embeddings.baseline_train_labels.astype("int32")
    embeddings.baseline_test_labels[embeddings.baseline_test_labels == "Y"] = 1
    embeddings.baseline_test_labels[embeddings.baseline_test_labels == "N"] = 0
    embeddings.baseline_test_labels = embeddings.baseline_test_labels.astype("int32")

    dcl = DummyClassifier(strategy="stratified")
    dcl.fit(embeddings.baseline_train_data, embeddings.baseline_train_labels)
    preds = dcl.predict(embeddings.baseline_test_data)
    accuracy = accuracy_score(y_true=embeddings.baseline_test_labels, y_pred=preds)
    logger.info("Baseline accuracy: {}".format(accuracy))

    mlp = MLPClassifier(hidden_layer_sizes=(100, 1), activation="relu", solver="adam", alpha=0.00001, batch_size=100,
                        learning_rate="adaptive", max_iter=100, shuffle=True, verbose=True, early_stopping=True)
    mlp.fit(embeddings.baseline_train_data, embeddings.baseline_train_labels)
    preds = mlp.predict(embeddings.baseline_test_data)
    accuracy = accuracy_score(y_true=embeddings.baseline_test_labels, y_pred=preds)
    logger.info("MLP classifier accuracy: {}".format(accuracy))
    df = pd.DataFrame({"labels": embeddings.baseline_test_labels, "predictions": preds})
    run_dir_path = join(getcwd(), "../output", run_dir)
    file = join(run_dir_path, "eval_mlp.csv")
    df.to_csv(file, sep=",", encoding="UTF-8")


def main():
    # configure logger & load properties
    logger, run_dir, log_filename = config_logger()
    props = load_properties()
    logger.info("Project's configuration: {}".format(json.dumps(props, sort_keys=True, indent=4)))
    logger.info("Configuration file is loaded!")

    # Load train/test datasets
    train_df, test_df = read_data(logger=logger, properties=props)
    embeddings, model = get_embeddings_and_model(logger=logger, properties=props, train_data=train_df,
                                                 test_data=test_df)
    model.train_siamese(train_data=embeddings.train_siamese_data, properties=props)
    model.test_siamese(test_data=embeddings.test_siamese_data, properties=props, run_dir=run_dir)
    baseline(embeddings, logger=logger, run_dir=run_dir)


if __name__ == '__main__':
    main()
