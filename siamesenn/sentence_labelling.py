import json
import logging
from datetime import datetime
from os import getcwd, mkdir
from os.path import join, exists

import numpy as np
import pandas as pd
import yaml
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

from siamesenn_keras import ClassificationNN
from huggingface_lm import HuggingFaceLanguageModel


def config_logger():
    """
    Creates a logger which writes both in a file and in the console

    Returns
        Logger: the logger object
    """
    if not exists(join(getcwd(), "output")):
        mkdir(join(getcwd(), "output"))
    run_dir = "run_%s" % datetime.now().strftime('%Y%m%d-%H%M%S')
    mkdir(join(getcwd(), "output", run_dir))
    mkdir(join(getcwd(), "output", run_dir, "logs"))
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    program_logger = logging.getLogger(__name__)

    program_logger.setLevel(logging.INFO)
    log_filename = 'logs_%s' % datetime.now().strftime('%Y%m%d-%H%M%S')
    run_dir_path = join(getcwd(), "output", run_dir, "logs")
    file_handler = logging.FileHandler("{0}/{1}.log".format(run_dir_path, log_filename))
    file_handler.setFormatter(log_formatter)
    program_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    program_logger.addHandler(console_handler)
    return program_logger, run_dir, log_filename


def load_properties():
    """
    Reads the properties file to load the configuration

    Returns
        | dict: a dictionary containing the project's configuration
    """

    resources_folder = join(getcwd(), "resources")
    properties_file = "properties.yaml" if exists(
        join(resources_folder, "properties.yaml")) else "example_properties.yaml"
    properties_path = join(getcwd(), "resources", properties_file)
    with open(properties_path, 'r') as f:
        return yaml.safe_load(f)


def read_data():
    data_folder = join(getcwd(), "resources", "data", "bert")
    train_file = join(data_folder, "train_new.csv")
    test_file = join(data_folder, "test_new.csv")
    train_data = pd.read_csv(train_file, index_col=None, header=None, sep="\t")
    test_data = pd.read_csv(test_file, index_col=None, header=None, sep="\t")
    return train_data, test_data


def get_train_test_data(train_df, test_df, existing_bert=False):
    train_sentences, test_sentences = list(train_df[0]), list(test_df[0])
    train_labels, test_labels = np.asarray(preprocess_labels(list(train_df[1]))), np.asarray(
        preprocess_labels(list(test_df[1])))
    if existing_bert:
        bert_train, bert_test = np.asarray(preprocess_embeddings(list(train_df[2]))), np.asarray(
            preprocess_embeddings(list(test_df[2])))
    else:
        hlm = HuggingFaceLanguageModel(batch_size=100, sequence_length=16)
        hlm.prepare_data(train_sentences, only_eval=True, train=True)
        bert_train = hlm.encode_eval_data()

        hlm = HuggingFaceLanguageModel(batch_size=100, sequence_length=16)
        hlm.prepare_data(test_sentences, only_eval=True, train=False)
        bert_test = hlm.encode_eval_data(train_data=False)

        save_new_bert(train_df, bert_train, join(getcwd(), "resources", "data", "bert", "train_new.csv"))
        save_new_bert(test_df, bert_test, join(getcwd(), "resources", "data", "bert", "test_new.csv"))

    siamese_train, siamese_test = np.asarray(preprocess_embeddings(list(train_df[3]))), np.asarray(
        preprocess_embeddings(list(test_df[3])))

    train_data = np.concatenate((bert_train, siamese_train), axis=1)
    test_data = np.concatenate((bert_test, siamese_test), axis=1)
    return train_data, test_data, train_labels, test_labels


def save_new_bert(df, bert_data, path):
    bert_data = list(bert_data)
    final_list = []
    for vector in bert_data:
        vector = list(vector)
        vector = [str(x) for x in vector]
        vector = " ".join(vector)
        final_list.append(vector)
    df[4] = final_list
    df.to_csv(path, sep="\t", header=None, index=None, encoding="UTF-8")


def preprocess_embeddings(string_vectors):
    final_list = []
    for string_vector in string_vectors:
        string_list = string_vector.split()
        final_list.append([float(x) for x in string_list])
    return final_list


def preprocess_labels(str_labels):
    return [int(x) for x in str_labels]


def performance(logger, true_labels, predicted_labels):
    logger.info("Accuracy: {}".format(accuracy_score(y_true=true_labels, y_pred=predicted_labels)))
    logger.info(
        "Macro Precision: {}".format(precision_score(y_true=true_labels, y_pred=predicted_labels, average="macro")))
    logger.info(
        "Micro Precision: {}".format(precision_score(y_true=true_labels, y_pred=predicted_labels, average="micro")))
    logger.info("Macro Recall: {}".format(recall_score(y_true=true_labels, y_pred=predicted_labels, average="macro")))
    logger.info("Micro Recall: {}".format(recall_score(y_true=true_labels, y_pred=predicted_labels, average="micro")))
    logger.info("Macro F: {}".format(f1_score(y_true=true_labels, y_pred=predicted_labels, average="macro")))
    logger.info("Micro F: {}".format(f1_score(y_true=true_labels, y_pred=predicted_labels, average="micro")))


def predictions_to_file(true_labels, predicted_labels, run_dir, filename="eval_keras_mlp.csv"):
    df = pd.DataFrame({"labels": true_labels, "predictions": predicted_labels})
    run_dir_path = join(getcwd(), "output", run_dir)
    file = join(run_dir_path, filename)
    df.to_csv(file, sep=",", encoding="UTF-8")


def main():
    # configure logger & load properties
    logger, run_dir, log_filename = config_logger()
    props = load_properties()
    logger.info("Project's configuration: {}".format(json.dumps(props, sort_keys=True, indent=4)))
    logger.info("Configuration file is loaded!")
    properties = load_properties()

    train_df, test_df = read_data()
    train_data, test_data, train_labels, test_labels = get_train_test_data(train_df, test_df)

    # np.random.seed(2020)
    # np.random.shuffle(train_data)
    input_dim = train_data.shape[1]
    classifier = ClassificationNN(properties=properties, input_dim=input_dim)
    classifier.train(train_data=train_data, train_labels=train_labels, properties=props, run_dir=run_dir,
                     log_filename=log_filename)
    true_labels, predicted_labels = classifier.test(test_data=test_data, test_labels=test_labels, properties=props)

    performance(logger=logger, true_labels=true_labels, predicted_labels=predicted_labels)
    predictions_to_file(true_labels=true_labels, predicted_labels=predicted_labels, run_dir=run_dir)


if __name__ == '__main__':
    main()
