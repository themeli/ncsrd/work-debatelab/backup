import itertools
from os import getcwd, mkdir
from os.path import join, exists

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as k_back
from tensorflow.keras import callbacks
from tensorflow.keras.backend import stack, argmin
from tensorflow.keras.layers import Dense, Dropout, Input
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import SGD, Adam


class KerasNN:
    @staticmethod
    def get_training_callbacks(properties, run_dir, log_filename, monitor_metric="categorical_accuracy"):
        """
        It saves the model after some period, stops it if there is no improvement after a predetermined number of epochs
        and modifies the learning rate if the improvement of the loss function is smaller than a threshold value.

        Args
            properties (dict): output folder and dnn characteristics
            monitor_metric (str): validation loss which is the monitoring measure for the callbacks

        Returns
            list: callbacks after the termination of the model training
        """
        output_folder = join(getcwd(), "output")
        model_folder_path = join(output_folder, "model")
        if not exists(model_folder_path):
            mkdir(model_folder_path)
        early_stopping_patience = int(properties["siamese_keras"]["epochs"] / 2)
        reduce_lr = int(properties["siamese_keras"]["epochs"] / 5)
        callbacks_list = [
            callbacks.ModelCheckpoint(join(model_folder_path, "model_weights.{epoch:02d}-{loss:.2f}.hdf5"),
                                      monitor=monitor_metric, verbose=0, save_weights_only=False,
                                      mode='auto', period=1),
            callbacks.EarlyStopping(monitor=monitor_metric, min_delta=0, patience=early_stopping_patience,
                                    verbose=0, mode='auto', baseline=None, restore_best_weights=False),
            callbacks.ReduceLROnPlateau(monitor=monitor_metric, factor=0.1,
                                        patience=reduce_lr, verbose=0, mode='auto',
                                        min_delta=0.0001, cooldown=0, min_lr=0),
            keras.callbacks.CSVLogger(
                join(getcwd(), "output", run_dir, "logs", log_filename), separator=',',
                append=True),
            keras.callbacks.TensorBoard(
                log_dir=join(getcwd(), "output", run_dir, "logs"),
                histogram_freq=0,
                write_graph=True,
                write_images=False,
                update_freq="epoch",
                profile_batch=2,
                embeddings_freq=0,
                embeddings_metadata=None,
            )
        ]

        return callbacks_list


class ClassificationNN(KerasNN):

    def __init__(self, properties, input_dim):
        hidden_layers = properties["siamese_keras"]["hidden_layers"]
        self.hidden_dim = hidden_layers[-1]
        classification = properties["label"]
        metrics = properties["siamese_keras"]["metrics"]
        learning_rate = properties["siamese_keras"]["lr"]
        decay = properties["siamese_keras"]["decay"]
        self.model = Sequential()
        self.classification = classification
        self.num_classes = 2 if classification == "binary" else 3
        for i, hidden in enumerate(hidden_layers):
            if type(hidden[0]) == int:
                hidden_units = hidden[0]
                activation = hidden[1]
            else:
                if self.classification == "binary":
                    hidden_units = 2
                else:
                    hidden_units = 4
                activation = hidden[0]
            self.model.add(Dense(hidden_units, activation=activation, input_dim=input_dim)) if i == 0 else \
                self.model.add(Dense(hidden_units, activation=activation))
            if len(hidden) == 3:
                self.model.add(Dropout(hidden[2]))
        optimizer = SGD(lr=learning_rate, decay=decay) if properties["siamese_keras"]["optimizer"] == "sgd" else \
            Adam(learning_rate=learning_rate, decay=decay)
        self.model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=metrics)

    def train(self, train_data, train_labels, properties, run_dir, log_filename):
        train_labels = keras.utils.to_categorical(train_labels, num_classes=self.num_classes)
        tf.random.set_seed(2019)
        self.model.fit(train_data, train_labels, epochs=properties["siamese_keras"]["epochs"],
                       batch_size=properties["siamese_keras"]["batch"], verbose=True, shuffle=True,
                       callbacks=self.get_training_callbacks(properties, run_dir, log_filename + ".log"))

    def test(self, test_data, test_labels, properties):
        result = self.model.predict(test_data)
        predicted_labels, probabilities = self.get_predicted_labels_and_probabilities(properties=properties,
                                                                                      predictions=result)
        return test_labels, predicted_labels

    @staticmethod
    def get_predicted_labels_and_probabilities(properties, predictions):
        """
        Finds the predicted labels for the movies and the corresponding probabilities of belonging to a class.

        Args

            properties (dict): classification
            predictions (tuple): the tuple created from the predict_proba function that returns a tuple with a
                                probability of belonging to a class

        Returns
            A list with the predicted labels and a list with their corresponding probabilities
        """
        predicted_labels = []
        probabilities = []
        predictions = list(predictions)
        for idx, prediction in enumerate(predictions):
            prediction = list(prediction)
            max_prediction = max(prediction)
            max_idx = prediction.index(max(prediction))
            if properties["label"] == "multi":
                max_idx = max_idx + 1
            predicted_labels.append(max_idx)
            probabilities.append(max_prediction)
        return predicted_labels, probabilities


class BertSiameseNN(KerasNN):

    def __init__(self, properties, input_dim, logger):
        margin = properties["siamese_keras"]["margin"]
        hidden_layers = properties["siamese_keras"]["hidden_layers"]
        self.hidden_dim = hidden_layers[-1]
        self.logger = logger
        classification = properties["label"]
        metrics = properties["siamese_keras"]["metrics"]
        learning_rate = properties["siamese_keras"]["lr"]

        self.model = Sequential()
        self.classification = classification
        self.margin = margin
        self.num_classes = 2 if classification == "binary" else 3

        for i, hidden in enumerate(hidden_layers):
            if len(hidden) == 1:
                continue
            hidden_units = hidden[0]
            activation = hidden[1]
            self.model.add(Dense(hidden_units, activation=activation, input_dim=input_dim)) if i == 0 else \
                self.model.add(Dense(hidden_units, activation=activation))
            if len(hidden) == 3:
                self.model.add(Dropout(hidden[2]))
        input_shape = (input_dim,)
        input_a, input_p, input_n = Input(input_shape), Input(input_shape), Input(input_shape)

        a = self.model(input_a)
        p = self.model(input_p)
        n = self.model(input_n)
        conc = keras.layers.concatenate([a, p, n], axis=1)

        self.final_model = Model(inputs=[input_a, input_p, input_n], outputs=conc)
        optimizer = SGD(lr=learning_rate) if properties["siamese_keras"]["optimizer"] == "sgd" else \
            Adam(learning_rate=learning_rate)
        self.final_model.compile(loss=self.triplet_loss, optimizer=optimizer, metrics=metrics)

    def triplet_loss(self, labels_true, out):
        self.logger.debug("True labels: {}".format(labels_true))
        h = int(out.shape.as_list()[-1] / 3)
        a = out[:, 0:h]
        p = out[:, h: 2 * h]
        n = out[:, 2 * h:]
        pos_dist = a - p
        neg_dist = a - n
        return k_back.mean(k_back.maximum(pos_dist - neg_dist + self.margin, 0.0))

    def train(self, train_data, properties, run_dir, log_filename):
        train_labels = train_data["labels"]
        a = train_data["anchors"]
        p = train_data["positives"]
        n = train_data["negatives"]
        input_data = [a, p, n]
        labels = keras.utils.to_categorical(train_labels, num_classes=self.num_classes)
        tf.random.set_seed(2019)
        self.final_model.fit(input_data, labels, epochs=properties["siamese_keras"]["epochs"],
                             batch_size=properties["siamese_keras"]["batch"], verbose=True, shuffle=True,
                             callbacks=self.get_training_callbacks(properties, run_dir, log_filename + ".log"))

    def test(self, test_data):
        """
        Takes the test vectors and the corresponding true labels and tests the performance of the model.

        Args
            test_data (ndarray): testing dataset
            true_labels (ndarray): testing labels

        Returns
            confusion_matrix: the confusion matrix of the testing
        """
        true_labels = test_data["labels"]
        a = test_data["anchors"]
        p = test_data["positives"]
        n = test_data["negatives"]
        # input_data = np.concatenate(a, p, n)
        input_data = [a, p, n]
        result = self.final_model.predict(input_data)
        h = int(result.shape[-1] / 3)
        a = result[:, 0:h]
        p = result[:, h: 2 * h]
        n = result[:, 2 * h:]
        pos_dist = self.euclidean_distance([a, p])
        neg_dist = self.euclidean_distance([a, n])
        grouped = stack([pos_dist, neg_dist], axis=1)
        predictions = argmin(grouped, axis=1)
        predictions = k_back.eval(predictions)
        predictions = list(itertools.chain.from_iterable(predictions))
        return true_labels, predictions

    def get_embeddings(self, data):
        true_labels = data["labels"]
        a = data["anchors"]
        p = data["positives"]
        n = data["negatives"]
        input_data = [a, p, n]
        result = self.final_model.predict(input_data)
        h = int(result.shape[-1] / 3)
        a = result[:, 0:h]
        return list(a)

    @staticmethod
    def euclidean_distance(inputs):
        assert len(inputs) == 2, \
            'Euclidean distance needs 2 inputs, %d given' % len(inputs)
        u, v = inputs
        return k_back.sqrt(k_back.sum(k_back.square(u - v), axis=-1, keepdims=True))
